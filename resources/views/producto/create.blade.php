@extends('layouts.app')

@section('content')
    <h1>Create a product</h1>

    <form method="POST" action="{{ route('productos.store') }}">
        @csrf
        <div class="form-row">
            <label>Titulo</label>
            <input class="form-control" type="text" name="Titulo" value="{{ old('Titulo') }}" required>
        </div>
        <div class="form-row">
            <label>Descripcion</label>
            <input class="form-control" type="text" name="Descripcion" value="{{ old('Descripcion') }}" required>
        </div>
        <div class="form-row">
            <label>Precio</label>
            <input class="form-control" type="number" min="1.00" step="0.01" name="Precio" value="{{ old('Precio') }}" required>
        </div>
        <div class="form-row">
            <label>Cantidad</label>
            <input class="form-control" type="number" min="0" name="Cantidad" value="{{ old('Cantidad') }}" required>
        </div>
        <div class="form-row">
            <label>Estado</label>
            <select class="custom-select" name="Estado" required>
                <option value="" selected>Seleccionar</option>
                <option {{ old('Estado') == 'Disponible' ? 'selected' : '' }} value="Disponible">Disponible</option>
                <option {{ old('Estado') == 'No disponible' ? 'selected' : '' }} value="No Disponible">No Disponible</option>
            </select>
        </div>
        <div class="form-row mt-3">
            <button type="submit" class="btn btn-primary btn-lg">Crear Producto</button>
        </div>
    </form>
@endsection
