@extends('layouts.app')
@section('content')

    <h1>Lista de productos</h1>

    <a class="btn btn-success mb-3" href="{{ route('productos.create') }}">Crear</a>



    @empty ($productos)
        <div class="alert alert-warning">
           The list of productos is empty
        </div>
     @else
       <div class="table-responsive">
          <table class="table table-striped">
              <thead class="thead-light">
                   <tr>
                      <th>Id</th>
                      <th>Titulo</th>
                      <th>Descripcion</th>
                      <th>Precio</th>
                      <th>Cantidad</th>
                      <th>Estado</th>
                      <th>Acciones</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($productos as $producto)
                        <tr>
                           <td>{{ $producto->id }}</td>
                           <td>{{ $producto->Titulo }}</td>
                           <td>{{ $producto->Descripcion }}</td>
                           <td>{{ $producto->Precio}}</td>
                           <td>{{ $producto->Cantidad }}</td>
                           <td>{{ $producto->Estado }}</td>
                           <td>
                            <a class="btn btn-link" href="{{ route('productos.show', ['producto' => $producto->id]) }}">Ver</a>
                            <a class="btn btn-link" href="{{ route('productos.edit', ['producto' => $producto->id]) }}">Editar</a>
                            <form method="POST " class="d-inline" action="{{ route('productos.destroy', ['producto' => $producto->id]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-link">Eliminar</button>
                            </form>
                           </td>
                        </tr>
                  @endforeach
              </tbody>
           </table>
       </div>
   @endempty
@endsection
