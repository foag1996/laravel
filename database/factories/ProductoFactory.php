<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
    return [
        'Titulo' => $faker->sentence(3),
        'Descripcion' => $faker->paragraph(1),
        'Precio' => $faker->randomFloat($maxDecimals = 2, $min = 3, $max = 100),
        'Cantidad' => $faker->numberBetween(1, 10),
        'Estado' => $faker->randomElement(['available', 'unavailable']),
    ];
});
