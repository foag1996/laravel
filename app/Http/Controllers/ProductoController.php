<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Producto;

class ProductoController extends Controller
{
    public function index()
    {
       return view('producto.index')->with([
           'productos'=>Producto::all(),
       ]);
    }

    public function create()
    {
        return view('producto.create');
    }

    public function store()
    {
        //reglas para hacer las validaciones
        $rules = [
            'Titulo' => ['required', 'max:255'],
            'Descripcion' => ['required', 'max:1000'],
            'Precio' => ['required', 'min:1'],
            'Cantidad' => ['required', 'min:0'],
            'Estado' => ['required', 'in:Disponible,No disponible'],//regla in para validar el estado
        ];

        //validaciones
        request()->validate($rules);

        if (request()->Estado == 'Disponible' && request()->Cantidad == 0) {
            //session()->put('error', 'Si esta disponible debe tener un producto');//muestra el error constantemente
            //session()->flash('error', 'Si esta disponible debe tener un producto'); //muestra el error por un determinado tiempo

            return redirect()
                ->back()
                ->withInput(request()->all())
                ->withErrors('Si esta disponible de existir un producto'); //mensaje de error
        }
        //forma 1 de guardar datos 
        //$producto = Producto::create([
            //'Titulo' => request()->Titulo,
            //'Descripcion' => request()->Descripcion,
           // 'Precio' => request()->Precio,
            //'Cantidad' => request()->Cantidad,
            //'Estado' => request()->Estado,
            //]);

            //forma 2 de guardar datos, depende del fillable datos masivos 
            $producto=Producto::create(request()->all());

            //return redirect()->back(); //redirecciona a la pagina anterior, mism pagina
            //return redirect()->action('ProductoController@index');//redirecciona a la accion del controlador
            return redirect()
            ->route('productos.index') //redirecciona a la ruta que queramos
            ->withSuccess("El producto con id {$producto->id} ha sido Creado");//mensaje de creado

    }

    public function show($producto)
    {
        $producto=Producto::findOrFail($producto);
        return view('producto.show')->with([
            'producto'=>$producto,
        ]);
    }

    public function edit($producto)
    {
        return view('producto.edit')->with([
            'producto' => Producto::findOrFail($producto),
        ]);
    }

    public function update($producto)
    {
        //reglas para hacer las validaciones
        $rules = [
            'Titulo' => ['required', 'max:255'],
            'Descripcion' => ['required', 'max:1000'],
            'Precio' => ['required', 'min:1'],
            'Cantidad' => ['required', 'min:0'],
            'Estado' => ['required', 'in:Disponible, No disponible'],//regla in para validar el estado
        ];

        //validaciones
        request()->validate($rules);

        $producto = Producto::findOrFail($producto);

        $producto->update(request()->all());



        return redirect()
        ->route('productos.index')
        ->withSuccess("El producto con id {$producto->id} ha sido Actualizado");//mensaje de actualizado
    }

    public function destroy($producto)
    {
        $producto = Producto::findOrFail($producto);
        $producto->delete();
        return redirect()
        ->route('productos.index')
        ->withSuccess("El producto con id {$producto->id} ha sido Eliminado");//mensaje de eliminado
    }
}
